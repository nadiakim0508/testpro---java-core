import Homework.FizzBuzzExercise;
import Homework.FizzBuzzRecursionExercise;
import Homework.IfExercise;
import Homework.PalindromeExercise;

public class Main {

    public static void main(String []args){
        new IfExercise().run();
        new PalindromeExercise().run();
        new FizzBuzzExercise().run();
        new FizzBuzzRecursionExercise().run();
        new Main(4);
    }
    
    private Main(int x) {
        System.out.println(x);
    }

}