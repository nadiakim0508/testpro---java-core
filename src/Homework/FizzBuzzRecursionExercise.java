package Homework;

public class FizzBuzzRecursionExercise {
    void printFizzBuzz(int n) {
        if (n > 0) {
            printFizzBuzz(n - 1);
            if (n % 15 == 0) {
                System.out.println("FizzBuzz");
            } else if (n % 3 == 0) {
                System.out.println("Fizz");
            } else if (n % 5 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(n);
            }
        }
    }
    public void run() {
        printFizzBuzz(100);
    }
}


