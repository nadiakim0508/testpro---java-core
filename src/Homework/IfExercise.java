package Homework;

public class IfExercise {
    public void run() {
        String x = "Have a nice day!:)";
        int xLength = x.length();
        if(xLength % 2 == 0)
        {
            System.out.println("String is even");
        }
        else
        {
            System.out.println("String is not even");
        }
    }
}
